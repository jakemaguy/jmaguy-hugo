---
title: "Pantera Returns"
date: 2022-07-16T01:37:40Z
author: "Jake Maguy"
draft: false
showFullContent: false
cover: https://www.memecreator.org/static/images/memes/4934792.jpg
tags: ['Pantera Reunion', 'Pantera', 'Phil Anselmo']
type: blog
---

So the legendary groove metal band Pantera is reuniting for a [2023 Tour](https://loudwire.com/zakk-wylde-anthrax-charlie-benante-pantera-reunion-lineup/).  Pantera is one of the most influential metal bands to come out of the 90's metal scene.  They brought an energy and aggression never before seen.  They were the first heavy metal band to have a #1 Billboard album.

{{< image src="https://townsquare.media/site/366/files/2014/01/1551589_10152187827044697_1368118280_n.jpg?w=980&q=75" alt="Hello Friend" position="center" style="border-radius: 8px;" style="width: 300px;">}}

In the 90s, Phil Anselmo was in his prime.  He would effortlessly put on a good show and dance around the stage like a maniac during performances.  As of writing this, Phil is now 54 years old.

{{< image src="https://mariskalrock.com/wp-content/uploads/2022/03/phil-anselmo-video-marzo-22.jpg" alt="Hello Friend" position="center" style="border-radius: 8px;" style="width: 300px;">}}

The tour will be done with the surviving members of Pantera.  Zakk Wylde, and Charlie Benante from Antrax, will be joining.  Zakk is a fantastic guitarist and will be able to do justice for Dimebag Darrell.

{{< figure src="https://stompboxbook.com/wp-content/uploads/2019/07/Dimebag-Darrell.jpg" alt="Hello Friend" position="center" style="border-radius: 8px;" caption="RIP" captionPosition="right" captionStyle="color: Black;" style="width: 400px">}}

I have mixed feelings about the tour.

## Pros
- Pantera will, and always will be, a kick ass band.  It's great that they're willing to play more
- Phil Anselmo still has a great voice for metal, even at 54 (as of 2022)
- They picked fantastic musicians to fill in for the deceased members

## Cons
- I don't think they will be writing new songs.  The reunion will be a nostalgia fest for old heads.
- They will not be able to match the energy of 90s Pantera.  This is not a knock against them personally, but it just wont be the same.

### Fun Fact
If you aren't aware, Phil has been included into an animation series called **Cooking Hostile** on youtube.  He voiced his own character on episode 4.  It's cool to see him participate in fan projects such as this.
{{< youtube Ay7_5Qq5WCc >}}

Phil Himself:
{{< youtube Ds5fnfz-DTY >}}
