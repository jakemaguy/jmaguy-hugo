---
title: "Teenage Dirtbag Is Stuck In My Head"
date: 2022-07-17T02:21:22Z
author: "Jake Maguy"
draft: false
cover: https://pbs.twimg.com/profile_images/1284188394554363904/30wSAlAe_400x400.jpg
tags: ['Wheatus', 'Teenage Dirtbag']
type: blog
---

Teenage Dirtbag is a song by Wheatus that was released in the year 2000.  It is two decades old now.  

Lets be real for a second:  Wheatus is a one hit wonder band.  This was their magnum opus.  I'm not familiar with any of their other songs.  This song was super popular back in the day.  For some reason, I've never heard of this song until recently.  I suppose I was hiding under a rock in the early 2000's.  This song was a huge hit when it was first released.

## The Backstory Behind Teenage Dirtbag
The Backstory is the most interesting aspect of the song.  The vocalist Brendan B. Brown grew up on Long Island in 84.  During this time, there was a troubled teen named Ricky Kasso who grew up in the same area as Brendan.  Ricky indulged in heavy drug use and satanic worship. 

There is a documentary/movie about him, and it's fascinating to see how his sanity began to unravel the more he got into satanism. 
{{< youtube BtP0x-Y1R4o >}}

Long Story Short:  Ricky abused drugs, was introduced to Satanism, Did a lot of drugs frequently, Stabbed a Friend of his in the woods in the name of Satan, killed himself in prison to avoid consequences of his actions. 

Ricky is the prime example of why:
{{< image src="https://media2.giphy.com/media/3o6ZtmxuHKqQaZJliE/200.gif" alt="Hello Friend" position="center" style="border-radius: 8px;" style="width: 300px;">}}

With that unpleasantness out of the way, the frontman of Wheatus read an article about Ricky in a magazine that labeled him a **Dirtbag**. Brendan liked a few bands that Ricky listened to like AC/DC, Iron Maiden, and Metallica.  When Ricky was arrested for the murder of his friend, he was wearing an AC/DC shirt.

Brendan knew that liking these bands did not make him a satanist.  He identified himself to be a teenage dirtbag at the time.  He explained: ```dirtbag is someone who does their own thing and is defiant.```

## About the Song
There's nothing truly remarkable about the song.  It's a standard pop rock song with a simple chord progression. The song is catchy as hell, and is fun to sing along with.  I guarantee if you play this at a party, 20 and 30 year olds alike will sing along in drunken glory.  The song is great in it's simplicity.  It serves a purpose and doesn't try to accomplish too many things at once.
{{< youtube FC3y9llDXuM >}}

My favorite part of the song is the verse that starts at [2:00](https://youtu.be/FC3y9llDXuM?t=122).  Brendan plays a guitar chord that he lets ring out for a second or two.  Whatever chord he's playing sounds huge.  I'm a fan of how it sounds sonically.  It sounds more full than the standard power chords we hear in the main chorus of the song.  This magical chord adds a new level of energy and excitement when I listen to the song.