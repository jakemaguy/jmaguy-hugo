---
title: "Jake Maguy"
date: 2022-07-15T04:21:01Z
draft: false
---


{{< figure src="/img/jmaguy.jpg" alt="Jake Maguy" position="center" style="border-radius: 8px;"  captionPosition="center" captionStyle="color: White;" style="width:200px; object-fit: cover; border-radius: 50%;">}} 
# Hi my name is Jake Maguy


I am a software engineer working at [Viasat](https://www.viasat.com/).  Welcome to my personal website. I will discuss programming projects that I'm looking into, as well as documenting lifestyle blogs.  Along with programming topics, you can expect to see content related to motorcycling, fishing, hiking, and music.

# Latest Posts:
